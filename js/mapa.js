var map = L.map('map').setView([51.505, -0.09], 2);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

cities = [
    {
        'name': 'Silicon valley',
        'lat': 37.442996,
        'lng': -122.154523
    },
    {
        'name': 'Guadalajara',
        'lat': 20.333333,
        'lng': -103.66666
    },
    {
        'name': 'Monterrey',
        'lat': 25.639784,
        'lng': -100.293102
    },
    {
        'name': 'CDMX',
        'lat': 19.320556,
        'lng': -99.151701
    }, 
    {
        'name': 'Honk Kong',
        'lat': 22.28552,
        'lng': 114.15769
    },
    {
        'name': 'MacCarthys Campeche',
        'lat': 19.8552507,
        'lng': -90.5284408
    }
]

console.log(cities.length)

for(var i = 0; i<cities.length; i++){
    console.log(cities[i].name)
    L.marker([cities[i].lat, cities[i].lng]).addTo(map)
    .bindPopup(cities[i].name)
    .openPopup();
}
