$(function(){
	console.log("Ready");
	var click_btn = $('#click_btn')
		url = ' https://pokeapi.co/api/v2/pokemon/pikachu/'
		pic = $('#pic');
		name_pokemon=$('#name_pokemon');
		name_pokemon_weight = $('#name_pokemon_weight');
		ataques = $('#ataques');

	click_btn.click(function(){
		console.log("Click");
		$.ajax({
			type: 'GET',
			url: url,
			success: function(data){
				console.log(data.sprites['back_default']);
				console.log(data)
				console.log(data.species['name']);
				console.log(data.weight);
				$.each(data.moves, function(i, val){
					console.log(i, "=>", val.move['name']);

				})
				//Datos para el usuario
				pic.html('<img src=' + data.sprites['back_default'] + '>');
				name_pokemon.html('<strong>' + data.species['name'] + '</strong>');
				name_pokemon_weight.html('<strong>' + data.weight + '</strong>')
				$.each(data.moves, function(i,val){
					ataques.append('<li>' + val.move['name'] + '</li>');
				})

			}
		})
	});
});